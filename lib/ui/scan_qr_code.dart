import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class ScanCRCode extends StatefulWidget {
  @override
  _ScanCRCodeState createState() => _ScanCRCodeState();
}

class _ScanCRCodeState extends State<ScanCRCode> {
  final GlobalKey qrKey = GlobalKey();
  String barcode = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Code Scan'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: QRView(
              key: qrKey,
              onQRViewCreated: (controller) {
                controller.scannedDataStream.listen((value) {
                  setState(() {
                    barcode = value;
                  });
                });
              },
            ),
          ),
          Expanded(
            child: Center(
              child: Text('Result: $barcode'),
            ),
          ),
        ],
      ),
    );
  }
}
