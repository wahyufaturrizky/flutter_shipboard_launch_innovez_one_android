/*
  start dummy loading and success
  This function is just for demo
  dummy loading and then show success popup
   */
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart';

void startLoading(context, String textMessage, bool backToPreviousPage) {
  _progressDialog(context);
  Timer(Duration(seconds: 2), () {
    Navigator.of(context, rootNavigator: true).pop();
    _buildShowDialog(context, textMessage, backToPreviousPage);
  });
}

Future _progressDialog(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return null;
          },
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      });
}

Future _buildShowDialog(
    BuildContext context, String textMessage, bool backToPreviousPage) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return null;
          },
          child: Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0)), //this right here
            child: Container(
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.fromLTRB(40, 20, 40, 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    textMessage,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16, color: CHARCOAL),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: LIGHT_BLUE_COLOR)),
                      onPressed: () {
                        Navigator.pop(context);

                        if (backToPreviousPage) {
                          FocusScope.of(context)
                              .unfocus(); // hide keyboard when press button
                          Navigator.pop(context);
                        }
                      },
                      padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
                      color: LIGHT_BLUE_COLOR,
                      textColor: Colors.white,
                      child: Text(
                        'OK'.toUpperCase(),
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      });
}
// end dummy loading and success
