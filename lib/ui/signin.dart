/*
This is signin page

Don't forget to add all images and sound used in this pages at the pubspec.yaml
 */

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/launch_job.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/main.dart';

class SigninPage extends StatefulWidget {
  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        // top blue background gradient
        Container(
          height: MediaQuery.of(context).size.height / 3.5,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            AppStateContainer.of(context).theme.primaryColor,
            AppStateContainer.of(context).theme.accentColor
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        ),
        // set your logo here
        Container(
            margin: EdgeInsets.fromLTRB(
                0, MediaQuery.of(context).size.height / 10, 0, 0),
            alignment: Alignment.topCenter,
            child: Image.asset('assets/images/logo_dark.png', height: 35)),
        ListView(
          children: <Widget>[
            // create form login
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 5,
              margin: EdgeInsets.fromLTRB(
                  30, MediaQuery.of(context).size.height / 3.5 - 60, 30, 0),
              color: AppStateContainer.of(context).theme.primaryColor,
              child: Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                  width: MediaQuery.of(context).size.width - 60,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 40,
                      ),
                      Center(
                        child: Text(
                          'SIGN IN',
                          style: TextStyle(
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor,
                              fontSize: 18,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        cursorColor:
                            AppStateContainer.of(context).theme.accentColor,
                        style: TextStyle(
                            color: AppStateContainer.of(context)
                                .theme
                                .accentColor),
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppStateContainer.of(context)
                                        .theme
                                        .accentColor)),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFCCCCCC)),
                            ),
                            labelText: 'Email',
                            labelStyle: TextStyle(
                                color: AppStateContainer.of(context)
                                    .theme
                                    .accentColor)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        cursorColor:
                            AppStateContainer.of(context).theme.accentColor,
                        style: TextStyle(
                            color: AppStateContainer.of(context)
                                .theme
                                .accentColor),
                        obscureText: true,
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppStateContainer.of(context)
                                        .theme
                                        .accentColor)),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFCCCCCC)),
                            ),
                            labelText: 'Password',
                            labelStyle: TextStyle(
                                color: AppStateContainer.of(context)
                                    .theme
                                    .accentColor)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        child: SizedBox(
                            width: double.maxFinite,
                            child: RaisedButton(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  side: BorderSide(
                                      color: AppStateContainer.of(context)
                                          .theme
                                          .accentColor)),
                              onPressed: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LaunchJob()));
                              },
                              padding: PADDING_BUTTON_FULL_WIDTH,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor,
                              textColor: WHITE_COLOR,
                              child: Text(
                                'LOGIN',
                                style: TextStyle(
                                    fontSize: FONT_SIZE_BUTTON_FULL_WIDTH,
                                    fontWeight:
                                        FONT_WEIGHT_BOLD_BUTTON_FULL_WIDTH,
                                    color: AppStateContainer.of(context)
                                        .theme
                                        .primaryColor),
                                textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                    ],
                  )),
            ),
            SizedBox(
              height: 50,
            ),
            SizedBox(
              height: 20,
            ),
          ],
        )
      ],
    ));
  }
}
