/*
This is launch job
divided to 2 part
- appBar()
- _buildListMenuJob()
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/mqtt_client.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/model/job_model.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/main.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/live_location.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/main_menu.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/reuseable/shimmer_loading.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/scan_qr_code.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/signin.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mqtt_client/mqtt_client.dart';

import 'package:mqtt_client/mqtt_server_client.dart';
import 'dart:io';

class LaunchJob extends StatefulWidget {
  @override
  _LaunchJobState createState() => _LaunchJobState();
}

class _LaunchJobState extends State<LaunchJob> {
  List<dynamic> listDataJob = [];
  MqttClient client;
  Job _job = null;
  var topic = "LSDS/tenant1/1753";

  bool _loading = true;
  String messageOnScroll = '';

  void _onConnectMQTTServer() {
    // onConnect
    connect().then((value) {
      if (value != null) {
        client = value;

        var isSubscribe = client?.subscribe(topic, MqttQos.atLeastOnce);

        if (isSubscribe != null) {
          final builder = MqttClientPayloadBuilder();
          builder.addString('Hello MQTT this me Wahyu Fatur Rizki Flutter');
          var isPublished = client?.publishMessage(
              topic, MqttQos.atLeastOnce, builder.payload);

          if (isPublished != null) {
            print('isPublished = $isPublished');
            setState(() {
              _loading = false;
            });
          }
        }
      }
    });
  }

  void _publish(String message) {}

  Future<MqttClient> connect() async {
    MqttServerClient client =
        MqttServerClient.withPort('10.10.45.186', 'flutter_client', 1883);
    client.logging(on: true);
    client.onConnected = onConnected;
    client.onDisconnected = onDisconnected;
    client.onUnsubscribed = onUnsubscribed;
    client.onSubscribed = onSubscribed;
    client.onSubscribeFail = onSubscribeFail;
    client.pongCallback = pong;

    final connMess = MqttConnectMessage()
        .withClientIdentifier("flutter_client")
        .authenticateAs("test", "test")
        .keepAliveFor(60)
        .withWillTopic('willtopic')
        .withWillMessage('My Will message')
        .startClean()
        .withWillQos(MqttQos.atLeastOnce);
    client.connectionMessage = connMess;
    try {
      print('Connecting');
      await client.connect();
    } catch (e) {
      print('Exception: $e');
      client.disconnect();
    }

    if (client.connectionStatus.state == MqttConnectionState.connected) {
      print('EMQX client connected');

      client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
        final MqttPublishMessage message = c[0].payload;
        final payload =
            MqttPublishPayload.bytesToStringAsString(message.payload.message);

        print(
            'Job Launch Client Received message:$payload from topic: ${c[0].topic}>');
      });

      client.published.listen((MqttPublishMessage message) {
        print('published');
        final payload =
            MqttPublishPayload.bytesToStringAsString(message.payload.message);
        print(
            'Job Launch Client Published message: $payload to topic: ${message.variableHeader.topicName}');
        Job.getJobdatas(payload).then((value) {
          setState(() {
            _job = value;
          });
        });
      });
    } else {
      print(
          'Job Launch EMQX client connection failed - disconnecting, status is ${client.connectionStatus}');
      client.disconnect();
      exit(-1);
    }

    return client;
  }

  @override
  void initState() {
    _onConnectMQTTServer();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onSearch() {}

  _onStartScroll(ScrollMetrics metrics) {
    setState(() {
      messageOnScroll = 'Scroll Start';
    });
  }

  _onUpdateScroll(ScrollMetrics metrics) {
    setState(() {
      messageOnScroll = 'Scroll Update';
    });
  }

  _onEndScroll(ScrollMetrics metrics) {
    setState(() {
      messageOnScroll = 'Scroll End';
    });
  }

  @override
  Widget build(BuildContext context) {
    print('_job');
    print(_job);
    return Scaffold(
      appBar: _appBar(),
      body: Container(
        decoration: BoxDecoration(
            color: AppStateContainer.of(context).theme.primaryColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: SPACER_VERTICAL,
            ),
            messageOnScroll != 'Scroll Update'
                ? Container(
                    margin: MARGIN_PAGE,
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: RaisedButton(
                            onPressed: () {},
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: AppStateContainer.of(context)
                                        .theme
                                        .accentColor)),
                            padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                            color: AppStateContainer.of(context)
                                .theme
                                .primaryColor,
                            textColor:
                                AppStateContainer.of(context).theme.accentColor,
                            child: Text(
                              'Current',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: AppStateContainer.of(context)
                                      .theme
                                      .accentColor),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 16),
                          child: RaisedButton(
                            onPressed: () {},
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: AppStateContainer.of(context)
                                        .theme
                                        .accentColor)),
                            padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                            color: AppStateContainer.of(context)
                                .theme
                                .primaryColor,
                            textColor:
                                AppStateContainer.of(context).theme.accentColor,
                            child: Text(
                              'History',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: AppStateContainer.of(context)
                                      .theme
                                      .accentColor),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
            messageOnScroll != 'Scroll Update'
                ? SizedBox(
                    height: SPACER_VERTICAL,
                  )
                : Container(),
            messageOnScroll != 'Scroll Update'
                ? Container(
                    margin: MARGIN_PAGE,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TextField(
                          onTap: onSearch,
                          maxLines: 1,
                          cursorColor:
                              AppStateContainer.of(context).theme.accentColor,
                          style: TextStyle(
                              fontSize: 16,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                          decoration: InputDecoration(
                            isDense: true,
                            suffixIcon: IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.search,
                                    color: AppStateContainer.of(context)
                                        .theme
                                        .accentColor)),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppStateContainer.of(context)
                                      .theme
                                      .accentColor),
                            ),
                            labelText: 'Search Job',
                            labelStyle: TextStyle(
                                color: AppStateContainer.of(context)
                                    .theme
                                    .accentColor),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: MENU_IMAGES_COLOR),
                            ),
                            contentPadding: EdgeInsets.all(8),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
            messageOnScroll != 'Scroll Update'
                ? SizedBox(
                    height: SPACER_VERTICAL,
                  )
                : Container(),
            Expanded(
                child: NotificationListener<ScrollNotification>(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  _onStartScroll(scrollNotification.metrics);
                } else if (scrollNotification is ScrollUpdateNotification) {
                  _onUpdateScroll(scrollNotification.metrics);
                } else if (scrollNotification is ScrollEndNotification) {
                  _onEndScroll(scrollNotification.metrics);
                }
              },
              child: (_loading == true)
                  ? Center(child: ShimmerList())
                  : ListView(
                      padding: EdgeInsets.only(bottom: 12),
                      children: List.generate(1, (index) {
                        return _buildLaunchJobList(index);
                      })),
            )),
            messageOnScroll != 'Scroll Update'
                ? Container(
                    margin: MARGIN_PAGE,
                    child: SizedBox(
                      width: double.maxFinite,
                      child: RaisedButton(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                                BORDER_RADIUS_BUTTON_FULL_WIDTH),
                            side: BorderSide(
                                color: AppStateContainer.of(context)
                                    .theme
                                    .accentColor)),
                        onPressed: () {},
                        padding: PADDING_BUTTON_FULL_WIDTH,
                        color: AppStateContainer.of(context).theme.accentColor,
                        textColor:
                            AppStateContainer.of(context).theme.primaryColor,
                        child: Text(
                          'START',
                          style: TextStyle(
                              fontSize: FONT_SIZE_BUTTON_FULL_WIDTH,
                              fontWeight: FONT_WEIGHT_BOLD_BUTTON_FULL_WIDTH),
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  Widget _buildLaunchJobList(index) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => LiveLocationPage()));
        Fluttertoast.showToast(
            msg: 'See detail job', toastLength: Toast.LENGTH_LONG);
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: AppStateContainer.of(context).theme.accentColor,
                  border: Border.all(
                      width: BORDER_WIDTH,
                      color: AppStateContainer.of(context).theme.accentColor),
                  borderRadius: BorderRadius.vertical(
                      top: Radius.circular(BORDER_RADIUS))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'launchJobData[index].endLoc',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .primaryColor),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                              'Manifest: ' + 'launchJobData[index].manifest',
                              style: TextStyle(
                                  fontSize: 13,
                                  color: AppStateContainer.of(context)
                                      .theme
                                      .primaryColor),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    child: Icon(
                      Icons.arrow_right_alt,
                      color: AppStateContainer.of(context).theme.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'launchJobData[index].endLoc',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .primaryColor),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                              'Manifest: ' + 'launchJobData[index].manifest',
                              style: TextStyle(
                                  fontSize: 13,
                                  color: AppStateContainer.of(context)
                                      .theme
                                      .primaryColor),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: AppStateContainer.of(context).theme.primaryColor,
                  border: Border.all(
                      width: BORDER_WIDTH,
                      color: AppStateContainer.of(context).theme.accentColor),
                  borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(BORDER_RADIUS))),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: PADDING_BOTTOM_CARD),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: BORDER_WIDTH, color: GREY_COLOR))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'From',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                        ),
                        Text(
                          '14:00',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: MARGIN_TOP_CARD),
                    padding: EdgeInsets.only(bottom: PADDING_BOTTOM_CARD),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'To',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                        ),
                        Text(
                          '14:00',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                        ),
                        Text(
                          'Start',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppStateContainer.of(context)
                                  .theme
                                  .accentColor),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SPACER_VERTICAL,
            ),
          ],
        ),
      ),
    );
  }

  // build appbar
  AppBar _appBar() {
    return AppBar(
      centerTitle: true,
      title: Text('Launch Job'),
      backgroundColor: AppStateContainer.of(context).theme.primaryColor,
      leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => MainMenu()));
          }),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.photo_camera),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScanCRCode(),
                ));
          },
        ),
        IconButton(
          icon: Icon(Icons.logout),
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => SigninPage(),
                ));
          },
        ),
      ],
    );
  }
}
