/*
This is contact us page

install plugin in pubspec.yaml
- url_launcher => used for call function and direct to email (https://pub.dev/packages/url_launcher)
 */

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart'
    show
        CHARCOAL,
        GRADIENT_BOTTOM,
        GRADIENT_TOP,
        MENU_IMAGES_COLOR,
        LIGHT_BLUE_COLOR;
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/reuseable/dummy_loading.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height / 3.5,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [GRADIENT_TOP, GRADIENT_BOTTOM],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
        ),
        Container(
            margin: EdgeInsets.fromLTRB(
                0, MediaQuery.of(context).size.height / 11, 0, 0),
            alignment: Alignment.topCenter,
            child: Image.asset('assets/images/logo_dark.png', height: 35)),
        ListView(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 5,
              margin: EdgeInsets.fromLTRB(
                  30, MediaQuery.of(context).size.height / 6, 30, 0),
              color: Colors.white,
              child: Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                  width: MediaQuery.of(context).size.width - 60,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                            onTap: () {
                              Navigator.of(context, rootNavigator: true).pop();
                            },
                            child: Icon(
                              Icons.close,
                              color: LIGHT_BLUE_COLOR,
                              size: 28,
                            )),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Text(
                          'CONTACT US',
                          style: TextStyle(
                              color: LIGHT_BLUE_COLOR,
                              fontSize: 18,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey[600])),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFCCCCCC)),
                            ),
                            labelText: 'Name',
                            labelStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 13)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey[600])),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFCCCCCC)),
                            ),
                            labelText: 'Email',
                            labelStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 13)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        maxLines: null,
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey[600])),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFCCCCCC)),
                            ),
                            labelText: 'Message',
                            labelStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 13)),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        child: SizedBox(
                            width: double.maxFinite,
                            child: RaisedButton(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  side: BorderSide(color: LIGHT_BLUE_COLOR)),
                              onPressed: () {
                                startLoading(context, 'Success', true);
                              },
                              padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
                              color: LIGHT_BLUE_COLOR,
                              textColor: Colors.white,
                              child: Text(
                                'SUBMIT',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                    ],
                  )),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
                margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
                alignment: Alignment.topCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        UrlLauncher.launch('mailto:+cs@email.com');
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.email, color: MENU_IMAGES_COLOR),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Email',
                                style: TextStyle(color: CHARCOAL),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'cs@email.com',
                                style: TextStyle(
                                    color: LIGHT_BLUE_COLOR,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        UrlLauncher.launch('tel:+62811888888');
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.phone_android, color: MENU_IMAGES_COLOR),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Phone',
                                style: TextStyle(color: CHARCOAL),
                              ),
                              SizedBox(height: 5),
                              Text(
                                '+62 811 888 888',
                                style: TextStyle(
                                    color: LIGHT_BLUE_COLOR,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                )),
            SizedBox(
              height: 20,
            )
          ],
        )
      ],
    ));
  }
}
