import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/mqtt_client.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'dart:io';

class MQTTPageSetting extends StatefulWidget {
  @override
  _MQTTPageSettingState createState() => _MQTTPageSettingState();
}

class _MQTTPageSettingState extends State<MQTTPageSetting> {
  MqttClient client;
  var topic = "LSDS/tenant1/1753";

  void _publish(String message) {
    final builder = MqttClientPayloadBuilder();
    builder.addString(message);
    client?.publishMessage(topic, MqttQos.atLeastOnce, builder.payload);
  }

  Future getLocalIpAddress() async {
    final interfaces = await NetworkInterface.list(
        type: InternetAddressType.IPv4, includeLinkLocal: true);
    try {
      // Try VPN connection first
      NetworkInterface vpnInterface =
          interfaces.firstWhere((element) => element.name == "tun0");
      return vpnInterface.addresses.first.address;
    } on StateError {
      // Try wlan connection next
      try {
        NetworkInterface interface =
            interfaces.firstWhere((element) => element.name == "wlan0");
        return interface.addresses;
      } catch (ex) {
        // Try any other connection next
        try {
          NetworkInterface interface = interfaces.firstWhere((element) =>
              !(element.name == "tun0" || element.name == "wlan0"));
          return interface.addresses;
        } catch (ex) {
          return null;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MQTT Connect'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('Connect'),
              onPressed: () => {
                connect().then((value) {
                  client = value;
                })
              },
            ),
            RaisedButton(
              child: Text('Subscribe'),
              onPressed: () {
                return {client?.subscribe(topic, MqttQos.atLeastOnce)};
              },
            ),
            RaisedButton(
              child: Text('Publish'),
              onPressed: () => {
                this._publish('Hello MQTT this me Wahyu Fatur Rizki Flutter')
              },
            ),
            RaisedButton(
              child: Text('Unsubscribe'),
              onPressed: () => {client?.unsubscribe(topic)},
            ),
            RaisedButton(
              child: Text('Disconnect'),
              onPressed: () => {client?.disconnect()},
            ),
            RaisedButton(
              child: Text('Test Network'),
              onPressed: () async {
                var ipAddress = await getLocalIpAddress();
                print('check IpAddress = $ipAddress');
              },
            ),
          ],
        ),
      ),
    );
  }
}

class IpAddress {}
