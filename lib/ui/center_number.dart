/*
This is edit center number page
 */

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/model/device_model.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/reuseable/dummy_loading.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/main.dart';

class EditCenterNumberPage extends StatefulWidget {
  @override
  _EditCenterNumberPageState createState() => _EditCenterNumberPageState();
}

class _EditCenterNumberPageState extends State<EditCenterNumberPage> {
  TextEditingController _etCenterNumber = TextEditingController();

  @override
  void initState() {
    _etCenterNumber = TextEditingController(text: '+62811888999');
    super.initState();
  }

  @override
  void dispose() {
    _etCenterNumber.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'CENTER NUMBER',
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          backgroundColor: AppStateContainer.of(context).theme.primaryColor,
        ),
        body: Container(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Text(
                'Serial Number',
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.grey[700],
                    fontWeight: FontWeight.normal),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                '1234567890',
                style: TextStyle(fontSize: 16, color: Colors.grey[700]),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'GPS Name',
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.grey[700],
                    fontWeight: FontWeight.normal),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                deviceData[0].devName,
                style: TextStyle(fontSize: 16, color: Colors.grey[700]),
              ),
              TextField(
                controller: _etCenterNumber,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[600])),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                    labelText: 'Center Number',
                    labelStyle:
                        TextStyle(color: Colors.grey[700], fontSize: 13)),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: SizedBox(
                    width: double.maxFinite,
                    child: RaisedButton(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(color: LIGHT_BLUE_COLOR)),
                      onPressed: () {
                        startLoading(
                            context, 'Edit Center Number Success', true);
                      },
                      padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
                      color: LIGHT_BLUE_COLOR,
                      textColor: Colors.white,
                      child: Text(
                        'SAVE',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    )),
              )
            ],
          ),
        ));
  }
}
