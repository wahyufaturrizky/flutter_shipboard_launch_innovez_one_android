/*
This is edit profile page
 */

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart'
    show LIGHT_BLUE_COLOR;
import 'package:fluttershipboardlaunchinnovezoneandroid/ui/reuseable/dummy_loading.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/main.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController _etName = TextEditingController();
  TextEditingController _etPhone = TextEditingController();

  @override
  void initState() {
    _etName = TextEditingController(text: 'Wahyu Fatur Rizki');
    _etPhone = TextEditingController(text: '+6282274586011');

    super.initState();
  }

  @override
  void dispose() {
    _etName.dispose();
    _etPhone.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'EDIT PROFILE',
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          backgroundColor: AppStateContainer.of(context).theme.primaryColor,
        ),
        body: Container(
          decoration: BoxDecoration(
              color: AppStateContainer.of(context).theme.primaryColor),
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              TextField(
                cursorColor: AppStateContainer.of(context).theme.accentColor,
                controller: _etName,
                style: TextStyle(
                    color: AppStateContainer.of(context).theme.accentColor),
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[600])),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                    labelText: 'Name',
                    labelStyle: TextStyle(
                        color: AppStateContainer.of(context).theme.accentColor,
                        fontSize: 13)),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                cursorColor: AppStateContainer.of(context).theme.accentColor,
                style: TextStyle(
                    color: AppStateContainer.of(context).theme.accentColor),
                keyboardType: TextInputType.phone,
                controller: _etPhone,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[600])),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                    labelText: 'Phone Number',
                    labelStyle:
                        TextStyle(color: Colors.grey[700], fontSize: 13)),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: SizedBox(
                    width: double.maxFinite,
                    child: RaisedButton(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(color: LIGHT_BLUE_COLOR)),
                      onPressed: () {
                        startLoading(context, 'Edit Profile Success', true);
                      },
                      padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
                      color: LIGHT_BLUE_COLOR,
                      textColor: Colors.white,
                      child: Text(
                        'SAVE',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    )),
              )
            ],
          ),
        ));
  }
}
