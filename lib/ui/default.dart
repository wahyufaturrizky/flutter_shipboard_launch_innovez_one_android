/*
This is default page
We used this pages first when we create new dart file
 */

import 'package:flutter/material.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/main.dart';

class DefaultPage extends StatefulWidget {
  @override
  _DefaultPageState createState() => _DefaultPageState();
}

class _DefaultPageState extends State<DefaultPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Default Page',
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          backgroundColor: AppStateContainer.of(context).theme.primaryColor,
        ),
        body: Container(
          child: Center(
            child: Text('This is Default Page'),
          ),
        ));
  }
}
