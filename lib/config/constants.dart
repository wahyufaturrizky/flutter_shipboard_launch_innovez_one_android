/*
this is constant pages
 */

import 'package:flutter/material.dart';

class CONSTANTS {
  static const SHARED_PREF_KEY_THEME = "theme_code";
}

class Themes {
  static const DARK_THEME_CODE = 0;
  static const LIGHT_THEME_CODE = 1;

  static final _dark = ThemeData(
    primarySwatch: MaterialColor(
      Colors.black.value,
      const <int, Color>{
        50: Colors.black12,
        100: Colors.black26,
        200: Colors.black38,
        300: Colors.black45,
        400: Colors.black54,
        500: Colors.black87,
        600: Colors.black87,
        700: Colors.black87,
        800: Colors.black87,
        900: Colors.black87,
      },
    ),
    accentColor: Colors.white,
  );

  static final _light = ThemeData(
    primarySwatch: MaterialColor(
      Colors.white.value,
      const <int, Color>{
        50: Colors.white10,
        100: Colors.white12,
        200: Colors.white24,
        300: Colors.white30,
        400: Colors.white54,
        500: Colors.white70,
        600: Colors.white70,
        700: Colors.white70,
        800: Colors.white70,
        900: Colors.white70,
      },
    ),
    accentColor: Colors.lightBlue,
  );

  static ThemeData getTheme(int code) {
    if (code == LIGHT_THEME_CODE) {
      return _light;
    } else {
      return _dark;
    }
  }
}

const String APP_NAME = 'Shipboard Launch';

// color for apps
const Color LIGHT_BLUE_COLOR = Color.fromARGB(255, 85, 210, 250);
const Color ASSENT_COLOR = Color(0xFFe75f3f);
const Color GRADIENT_TOP = Color.fromARGB(255, 85, 210, 250);
const Color GRADIENT_BOTTOM = Color(0xFF0299e2);
const Color WHITE_COLOR = Color(0xFFFFFFFF);
const Color GREY_COLOR = Color(0xFFE0E0E0);

const Color MENU_IMAGES_COLOR = Color(0xFF057DC3);
const Color MAPS_IMAGES_COLOR = Color(0xFF0a4349);
const Color CHARCOAL = Color(0xFF515151);
const Color OCEAN = Color(0xFF00ace9);

// image picture
const String IMAGES_URL =
    'https://cdn.shopify.com/s/files/1/1898/4483/files/never-run-out.png';

// Margin Page
const EdgeInsets MARGIN_PAGE = EdgeInsets.fromLTRB(20, 0, 20, 0);
const double SPACER_VERTICAL = 10;

// Border
const double BORDER_RADIUS = 6;
const double BORDER_WIDTH = 2.0;

// Padding Card Launch Job
const double PADDING_BOTTOM_CARD = 10.0;
const double MARGIN_TOP_CARD = 10.0;

// Button Full Width
const double BORDER_RADIUS_BUTTON_FULL_WIDTH = 10.0;
const double FONT_SIZE_BUTTON_FULL_WIDTH = 16;
const FontWeight FONT_WEIGHT_BOLD_BUTTON_FULL_WIDTH = FontWeight.bold;
const EdgeInsets PADDING_BUTTON_FULL_WIDTH = EdgeInsets.fromLTRB(0, 12, 0, 12);
