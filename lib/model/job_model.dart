import 'package:xml2json/xml2json.dart';
import 'dart:convert';

class Job {
  String resourceId;
  String msgId;
  String msgDt;
  String msgSrc;
  String msgDest;
  String msgType;
  String jobId;
  String gpsLatitude;
  String gpsLongitude;
  String reqAck;
  String tenantId;
  String detail;

  Job(
      {this.gpsLatitude,
      this.gpsLongitude,
      this.jobId,
      this.msgDest,
      this.msgDt,
      this.msgId,
      this.msgSrc,
      this.msgType,
      this.reqAck,
      this.resourceId,
      this.detail,
      this.tenantId});

  factory Job.createJob(Map<String, dynamic> object) {
    return Job(
      resourceId: object['resource_id'],
      msgId: object['msg_id'],
      msgDt: object['msg_dt'],
      msgSrc: object['msg_src'],
      msgDest: object['msg_dest'],
      msgType: object['msg_type'],
      jobId: object['job_id'],
      gpsLatitude: object['gps_latitude'],
      gpsLongitude: object['gps_longitude'],
      reqAck: object['req_ack'],
      tenantId: object['tenant_id'],
      detail: object['detail'],
    );
  }

  static Future<Job> getJobdatas(String dataStringXml) async {
    final Xml2Json xml2Json = Xml2Json();

    try {
      var getData = dataStringXml;
      xml2Json.parse(getData);

      // toParker
      var jsonDatatoParker = xml2Json.toParker();
      var datatoParker = json.decode(jsonDatatoParker);

      // toParker
      print('data toParker XML to Json');
      print(datatoParker);

      var dataJobLaunch = (datatoParker as Map<String, dynamic>)['Message'];

      print('dataJobLaunch check = ');
      print(dataJobLaunch);

      return Job.createJob(dataJobLaunch);
    } catch (e) {
      print('Error XML to Json = $e');
    }
  }
}
