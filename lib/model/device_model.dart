import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:fluttershipboardlaunchinnovezoneandroid/config/constants.dart';

class DeviceModel {
  int id;
  String sn;
  String devName;
  String manifest;
  String startLoc;
  String endLoc;
  String devDesc;
  String devStatus;
  Color color;
  int stop;
  int power;
  String iconName;
  var devIcon;
  double gpsLat;
  double gpsLong;
  LatLng gpsPosition;
  String speed;
  String date;

  DeviceModel(
      {this.id,
      this.sn,
      this.devName,
      this.startLoc,
      this.endLoc,
      this.devDesc,
      this.devStatus,
      this.color,
      this.stop,
      this.power,
      this.iconName,
      this.devIcon,
      this.gpsLat,
      this.gpsLong,
      this.gpsPosition,
      this.speed,
      this.manifest,
      this.date});

  DeviceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sn = json['sn'];
    devName = json['devName'];
    startLoc = json['startLoc'];
    endLoc = json['endLoc'];
    devDesc = json['devDesc'];
    devStatus = json['devStatus'];
    color = json['color'];
    stop = json['stop'];
    power = json['power'];
    iconName = json['iconName'];
    devIcon = json['devIcon'];
    gpsLat = json['gpsLat'].toDouble();
    gpsLong = json['gpsLong'].toDouble();
    gpsPosition = json['gpsPosition'];
    speed = json['speed'];
    date = json['date'];
    manifest = json['manifest'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['startLoc'] = this.startLoc;
    data['endLoc'] = this.endLoc;
    data['sn'] = this.sn;
    data['devName'] = this.devName;
    data['devDesc'] = this.devDesc;
    data['devStatus'] = this.devStatus;
    data['color'] = this.color;
    data['stop'] = this.stop;
    data['power'] = this.power;
    data['iconName'] = this.iconName;
    data['devIcon'] = this.devIcon;
    data['gpsLat'] = this.gpsLat;
    data['gpsLong'] = this.gpsLong;
    data['gpsPosition'] = this.gpsPosition;
    data['speed'] = this.speed;
    data['date'] = this.date;
    data['manifest'] = this.manifest;
    return data;
  }
}

List<DeviceModel> deviceData = [
  DeviceModel(
    id: 1,
    sn: '7028129300',
    devName: 'B 2455 UJD',
    devDesc: 'I put this GPS Tracker in my Honda car',
    devStatus: 'Online',
    color: Colors.lightGreen[700],
    stop: 0,
    power: 90,
    iconName: 'car',
    devIcon: Icons.directions_car,
    gpsLat: -6.168033,
    gpsLong: 106.900467,
    gpsPosition: LatLng(-6.168033, 106.900467),
    speed: '51 Km/h',
    date: '2020-07-15 09:02:31',
  ),
  DeviceModel(
    id: 2,
    sn: '7028129300',
    devName: 'My Mom',
    devDesc: 'I give this GPS Tracker to my mom',
    devStatus: 'Subscribe Expired',
    color: Colors.red[900],
    stop: 1,
    power: 73,
    iconName: 'elderly',
    devIcon: AssetImage('assets/images/elderly.png'),
    gpsLat: -6.164770,
    gpsLong: 106.900630,
    gpsPosition: LatLng(-6.164770, 106.900630),
    speed: '21 Km/h',
    date: '2020-07-14 19:23:27',
  ),
  DeviceModel(
    id: 3,
    sn: '4209995000',
    devName: 'B 4245 UTR',
    devDesc: 'I put this GPS Tracker in my motorcycle',
    devStatus: 'Power Saving',
    color: OCEAN,
    stop: 1,
    power: 9,
    iconName: 'bike',
    devIcon: Icons.motorcycle,
    gpsLat: -6.158637,
    gpsLong: 106.906376,
    gpsPosition: LatLng(-6.158637, 106.906376),
    speed: '34 Km/h',
    date: '2020-07-15 11:05:01',
  ),
];

List<DeviceModel> launchJobData = [
  DeviceModel(
    id: 1,
    startLoc: 'WCP',
    endLoc: 'APOLLO ACE',
    manifest: '2 Pax',
  ),
  DeviceModel(
    id: 1,
    startLoc: 'WCP',
    endLoc: 'APOLLO ACE',
    manifest: '2 Pax',
  ),
];
