# fluttershipboardlaunchinnovezoneandroid

GPS Tracking UI Kit

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Software / Engine use :
Flutter 1.20.1
Android Studio 4.0

List of plugin version :
fluttertoast: ^4.0.1
shimmer: ^1.1.1
package_info: ^0.4.1
cached_network_image: ^2.2.0+1
audioplayers: ^0.15.1
flutter_html: ^0.11.1
url_launcher: ^5.4.2
image_picker: ^0.6.4
image_cropper: ^1.3.0
permission_handler: ^4.4.0+hotfix.2
google_maps_flutter: ^0.5.29+1
location: ^3.0.2
geocoder: ^0.2.1
intl: ^0.16.1
date_format: ^1.0.8
